# Jupyter Notebook

#### This project allows to install and use the jupyter notebook locally by running the installation script only.
#### The folder `notebooks` presents tests for own studies.

### Jupyter 
https://jupyter.org/try

### Jupyter Documentation
https://jupyter.readthedocs.io/en/latest/index.html

### IPython kernel 
https://nbviewer.jupyter.org/github/ipython/ipython/blob/4.0.x/examples/IPython%20Kernel/Index.ipynb

### Tutorial
https://www.datacamp.com/community/tutorials/tutorial-jupyter-notebook

### How to use:

#### 1.  Open the terminal.

#### 2.  Clone the repository:
    ```
    git clone https://gitlab.com/suelen_regina/jupyter-notebook.git
    ```
#### 3.  Enter in directory:
    ```
    cd jupyter-notebook
    ```
#### 4.  Run the environment script:
    ```
    bash ./jupyter-install.sh
    ```    
#### 5.  Enable the environment if everything installed correctly:
    ```
    source jupyNote/bin/activate
    ```
#### 6.  To run a notebook: 
	```
    jupyter notebook (OR including '--no-browser' flag) 
    ```