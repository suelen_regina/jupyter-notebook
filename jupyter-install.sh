#!/bin/bash
# By Filippo Squillace - https://gist.github.com/fsquillace/721015d093c7e98faa284bc2d7bed3f8
# By Igor Morgado - https://github.com/igormorgado/introducaopython
# Provides all the basic packages and kernels to have a complete Jupyter notebook ready!

# First, it needs to install/verify python language, as well as the virtual environment

erro() {
	echo >&2 "ERRO: $@. Saindo."
	exit 1
}

# Basic Python
command -v python3 >/dev/null 2>&1 || erro "Python3 don't installed!"
command -v pip3 >/dev/null 2>&1 || erro "PIP3 don't installed!"

# Verifying the virtual envorinment
command -v virtualenv > /dev/null 2>&1
RETVAL=$?

if [ "${RETVAL}" -ne 0 ]
then
	pip3 install virtualenv || sudo apt install python3-venv || erro "We couldn't install the virtual environment package."
fi

# Creating the virtual environment
virtualenv -p /usr/bin/python3 jupyNote || python3 -m venv jupyNote || erro "We couldn't create the virtual environment."

# Activating the virtual environment
source jupyNote/bin/activate || erro "We couldn't activate the virtual environment."

# The installation includes:
# - scientific packages
# - ipython tab completion
# - bash kernel
# - ipywidgets
#
# To run a notebook:
# jupyter notebook --no-browser

set -e

# Jupyter - https://jupyter.org/try
# Jupyter Documentation - https://jupyter.readthedocs.io/en/latest/index.html
## IPython kernel - https://nbviewer.jupyter.org/github/ipython/ipython/blob/4.0.x/examples/IPython%20Kernel/Index.ipynb
pip3 install jupyter || erro "We couldn't install the jupyter."

# Basic kernels
## Bash kernel - https://github.com/takluyver/bash_kernel
pip3 install bash_kernel || erro "We couldn't install the bash_kernel."
python -m bash_kernel.install

## Ipywidgets - https://ipywidgets.readthedocs.io/en/latest/user_guide.html
pip3 install ipywidgets || erro "We couldn't install the ipywidgets."
jupyter nbextension enable --py widgetsnbextension

# Scientific python packages
pip3 install numpy scipy matplotlib pandas scikit-learn bokeh || erro "We couldn't install some scientific python package."

# IPython shift+tab completion
pip3 install pyreadline || erro "We couldn't install the pyreadline."

clear
cat << EOF

EOF
